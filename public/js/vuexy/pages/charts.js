// var dataset;

// function initPie(series) {
// 	// return series;
// 	dataset = series;
// }

var radialcustomangeloptions = {
	series: [cowo, cewe],
	chart: {
		height: 390,
		type: 'radialBar',
	},
	plotOptions: {
		radialBar: {
			offsetY: 0,
			startAngle: 0,
			endAngle: 270,
			hollow: {
				margin: 5,
				size: '30%',
				background: 'transparent',
				image: undefined,
			},
			dataLabels: {
				name: {
					show: false,
				},
				value: {
					show: false,
				}
			}
		}
	},
	colors: ['#1ab7ea', '#39539E'],
	labels: ['Laki-Laki', 'Perempuan'],
	legend: {
		show: true,
		floating: true,
		fontSize: '16px',
		position: 'left',
		offsetX: 160,
		offsetY: 15,
		labels: {
			useSeriesColors: true,
		},
		markers: {
			size: 0
		},
		formatter: function(seriesName, opts) {
			return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
		},
		itemMargin: {
			vertical: 3
		}
	},
	responsive: [{
		breakpoint: 480,
		options: {
			legend: {
				show: false
			}
		}
	}]
};

var radialgradientoptions = {
	series: [75],
	chart: {
		height: 350,
		type: 'radialBar',
		toolbar: {
			show: false
		}
	},
	plotOptions: {
		radialBar: {
		startAngle: -135,
		endAngle: 225,
		hollow: {
			margin: 0,
			size: '70%',
			background: '#fff',
			image: undefined,
			imageOffsetX: 0,
			imageOffsetY: 0,
			position: 'front',
			dropShadow: {
				enabled: true,
				top: 3,
				left: 0,
				blur: 4,
				opacity: 0.24
			}
		},
		track: {
			background: '#fff',
			strokeWidth: '67%',
			margin: 0, // margin is in pixels
			dropShadow: {
				enabled: true,
				top: -3,
				left: 0,
				blur: 4,
				opacity: 0.35
			}
		},
	
		dataLabels: {
			show: true,
			name: {
				offsetY: -10,
				show: true,
				color: '#888',
				fontSize: '17px'
			},
			value: {
				formatter: function(val) {
					return parseInt(val);
				},
				color: '#111',
				fontSize: '36px',
				show: true,
			}
		}
		}
	},
	fill: {
		type: 'gradient',
		gradient: {
			shade: 'dark',
			type: 'horizontal',
			shadeIntensity: 0.5,
			gradientToColors: ['#ABE5A1'],
			inverseColors: true,
			opacityFrom: 1,
			opacityTo: 1,
			stops: [0, 100]
		}
	},
	stroke: {
		lineCap: 'round'
	},
	labels: ['Percent'],
};

var radialwithimageoptions = {
	series: [67],
	chart: {
		height: 350,
		type: 'radialBar',
	},
	plotOptions: {
		radialBar: {
		hollow: {
			margin: 15,
			size: '70%',
			image: 'img/vuexy/icons/clock.png',
			imageWidth: 64,
			imageHeight: 64,
			imageClipped: false
		},
		dataLabels: {
			name: {
				show: false,
				color: '#fff'
			},
			value: {
				show: true,
				color: '#333',
				offsetY: 70,
				fontSize: '22px'
			}
		}
		}
	},
	fill: {
		type: 'image',
		image: {
			src: ['img/vuexy/pages/charts/abstrak1.jpg'],
		}
	},
	stroke: {
		lineCap: 'round'
	},
	labels: ['Volatility'],
};

var mixedlinecolumnoptions = {
	series: [{
		name: 'Income',
		type: 'column',
		data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6]
	}, {
		name: 'Cashflow',
		type: 'column',
		data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5]
	}, {
		name: 'Revenue',
		type: 'line',
		data: [20, 29, 37, 36, 44, 45, 50, 58]
	}],
	chart: {
		height: 350,
		type: 'line',
		toolbar: {
			show: false
		},
		stacked: false
	},
	dataLabels: {
		enabled: false
	},
	stroke: {
		width: [1, 1, 4]
	},
	title: {
		text: 'XYZ - Stock Analysis (2009 - 2016)',
		align: 'left',
		offsetX: 110
	},
	xaxis: {
		categories: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016],
	},
	yaxis: [{
		axisTicks: {
			show: true,
		},
		axisBorder: {
			show: true,
			color: '#008FFB'
		},
		labels: {
			style: {
				colors: '#008FFB',
			}
		},
		title: {
			text: "Income (thousand crores)",
			style: {
				color: '#008FFB',
			}
		},
		tooltip: {
			enabled: true
		}
	},
	{
		seriesName: 'Income',
		opposite: true,
		axisTicks: {
			show: true,
		},
		axisBorder: {
			show: true,
			color: '#00E396'
		},
		labels: {
			style: {
				colors: '#00E396',
			}
		},
		title: {
			text: "Operating Cashflow (thousand crores)",
			style: {
				color: '#00E396',
			}
		},
	},
	{
		seriesName: 'Revenue',
		opposite: true,
		axisTicks: {
			show: true,
		},
		axisBorder: {
			show: true,
			color: '#FEB019'
		},
		labels: {
			style: {
				colors: '#FEB019',
			},
		},
		title: {
			text: "Revenue (thousand crores)",
			style: {
				color: '#FEB019',
			}
		}
		},
	],
	tooltip: {
		fixed: {
			enabled: true,
			position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
			offsetY: 30,
			offsetX: 60
		},
	},
	legend: {
		horizontalAlign: 'left',
		offsetX: 40
	}
};

var radarmultipleoptions = {
	series: [{
		name: 'Series 1',
		data: [80, 50, 30, 40, 100, 20],
	}, {
		name: 'Series 2',
		data: [20, 30, 40, 80, 20, 80],
	}, {
		name: 'Series 3',
		data: [44, 76, 78, 13, 43, 10],
	}],
	chart: {
		height: 350,
		type: 'radar',
		dropShadow: {
			enabled: true,
			blur: 1,
			left: 1,
			top: 1
		}
	},
	title: {
		text: 'Radar Chart - Multi Series'
	},
	stroke: {
		width: 2
	},
	fill: {
		opacity: 0.1
	},
	markers: {
		size: 0
	},
	xaxis: {
		categories: ['2011', '2012', '2013', '2014', '2015', '2016']
	}
};

var donateupdateoptions = {
	series: [cowo, cewe],
	chart: {
		width: 380,
		type: 'donut',
	},
	dataLabels: {
		enabled: false
	},
	labels: ['Laki-Laki', 'Perempuan'],
	responsive: [{
		breakpoint: 480,
		options: {
			chart: {
				width: 200
			},
			legend: {
				show: false
			}
		}
	}],
	legend: {
		position: 'right',
		offsetY: 0,
		height: 230,
	}
};

var simplepieoptions = {
	series: [cowo, cewe],
	// series: dataset,
	// series: [3, 3],
	chart: {
		width: 380,
		type: 'pie',
	},
	// labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
	labels: ['Laki-Laki', 'Perempuan'],
	responsive: [{
		breakpoint: 480,
		options: {
			chart: {
				width: 200
			},
			legend: {
				position: 'bottom'
			}
		}
	}]
	// series: [3, 3],
    // chart: {
    // 	width: 380,
    // 	type: 'pie',
    // },
    // labels: ['Team A', 'Team B'],
    // responsive: [{
    //     breakpoint: 480,
    //     options: {
    //         chart: {
    //           	width: 200
    //         },
    //         legend: {
    //           	position: 'bottom'
    //         }
    //     }
    // }]
};

var userdonatoptions = {
	chart: {
		type: 'donut',
		height: 120,
		toolbar: {
		  show: false
		}
	},
	dataLabels: {
		enabled: false
	},
	series: [cowo, cewe],
	legend: { show: false },
	// comparedResult: [2, -3, 8],
	comparedResult: [5, 5],
	labels: ['Laki-Laki', 'Perempuan'],
	stroke: { width: 0 },
	colors: [(cowo > cewe) ? window.colors.solid.success : '#28c76f66', (cowo > cewe) ? '#28c76f66' : window.colors.solid.success],
	grid: {
		padding: {
			right: -20,
			bottom: -8,
			left: -20
		}
	},
	plotOptions: {
		pie: {
			// startAngle: 10,
			donut: {
				labels: {
					show: true,
					name: {
						offsetY: 13
					},
					value: {
						offsetY: -17,
						// formatter: function (val) {
						// 	// console.log(val);
						// 	// return (parseInt(val)/(cowo + cewe) * 100) + '%';
						// 	return val;
						// }
					},
					total: {
						show: true,
						offsetY: 15,
						label: (cowo > cewe) ? 'Laki-Laki' : 'Perempuan',
						fontSize: '100px',
						// console.log('test')
						// fontFamily: 'Helvetica, Arial, sans-serif',
						fontWeight: 100,
						formatter: function (w) {
							return (cowo > cewe) ? cowo : cewe;
						}
					}
				}
			}
		}
	},
	responsive: [
		{
			breakpoint: 1325,
			options: {
				chart: {
					height: 100
				}
			}
		},
		{
			breakpoint: 1200,
			options: {
				chart: {
					height: 120
				}
			}
		},
		{
			breakpoint: 1045,
			options: {
				chart: {
					height: 100
				}
			}
		},
		{
			breakpoint: 992,
			options: {
				chart: {
					height: 120
				}
			}
		}
	]
};

var custombaroptions = {
	series: [{
	data: [cowo, cewe]
  }],
	chart: {
	type: 'bar',
	height: 380
  },
  plotOptions: {
	bar: {
	  barHeight: '100%',
	  distributed: true,
	  horizontal: true,
	  dataLabels: {
		position: 'bottom'
	  },
	}
  },
  colors: ['#f48024', '#69d2e7'],
  dataLabels: {
	enabled: true,
	textAnchor: 'start',
	style: {
	  colors: ['#fff']
	},
	formatter: function (val, opt) {
	  return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
	},
	offsetX: 0,
	dropShadow: {
	  enabled: true
	}
  },
  stroke: {
	width: 1,
	colors: ['#fff']
  },
  xaxis: {
	categories: ['Laki-Laki', 'Perempuan'],
  },
  yaxis: {
	labels: {
	  show: false
	}
  },
//   title: {
// 	  text: 'Custom DataLabels',
// 	  align: 'center',
// 	  floating: true
//   },
//   subtitle: {
// 	  text: 'Category Names as DataLabels inside bars',
// 	  align: 'center',
//   },
  tooltip: {
	theme: 'dark',
	x: {
	  show: false
	},
	y: {
	  title: {
		formatter: function () {
		  return ''
		}
	  }
	}
  }
};

var basicpolarareaoptions = {
	series: [cowo, cewe],
	chart: {
		type: 'polarArea',
		height: 380,
	},
	labels: ['Laki-Laki', 'Perempuan'],
	stroke: {
		colors: ['#fff']
	},
	fill: {
		opacity: 0.8
	},
	responsive: [{
		breakpoint: 480,
		options: {
		chart: {
			width: 200
		},
		legend: {
			position: 'bottom'
		}
		}
	}]
};

var basiccolumnoptions = {
	// series: [{
	// 	name: 'Laki-Laki',
	// 	data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
	// }, {
	// 	name: 'Revenue',
	// 	data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
	// }, {
	// 	name: 'Free Cash Flow',
	// 	data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
	// }],
	// series: [{
	// 	name: 'Laki-Laki',
	// 	data: [
	// 		parseInt(kota['surabaya']['cowo']), 
	// 		parseInt(kota['sidoarjo']['cowo']), 
	// 		parseInt(kota['mojokerto']['cowo'])
	// 	]
	// }, {
	// 	name: 'Perempuan',
	// 	data: [
	// 		parseInt(kota['surabaya']['cewe']),
	// 		parseInt(kota['sidoarjo']['cewe']),
	// 		parseInt(kota['mojokerto']['cewe'])
	// 	]
	// }],
	series: [{
		name: 'Laki-Laki',
		data: [
			kota['surabaya']['cowo'], 
			kota['sidoarjo']['cowo'], 
			kota['mojokerto']['cowo']
		]
	}, {
		name: 'Perempuan',
		data: [
			kota['surabaya']['cewe'],
			kota['sidoarjo']['cewe'],
			kota['mojokerto']['cewe']
		]
	}],
	chart: {
		type: 'bar',
		height: 350
	},
	plotOptions: {
		bar: {
			horizontal: false,
			columnWidth: '55%',
			endingShape: 'rounded'
		},
	},
	dataLabels: {
		enabled: false
	},
	// stroke: {
	// 	show: true,
	// 	width: 2,
	// 	colors: ['transparent']
	// },
	xaxis: {
		categories: ['Surabaya', 'Sidoarjo', 'Mojokerto'],
	},
	yaxis: {
		title: {
			text: 'Jumlah User'
		}
	},
	// fill: {
	// 	opacity: 1
	// },
	// tooltip: {
	// 	y: {
	// 		formatter: function (val) {
	// 			return "$ " + val + " thousands"
	// 		}
	// 	}
	// }
	// series: [{
	// 	data: [cowo, cewe]
	// }],
	// chart: {
	// 	height: 350,
	// 	type: 'bar',
	// 	events: {
	// 		click: function(chart, w, e) {
	// 		// console.log(chart, w, e)
	// 		}
	// 	}
	// },
	// colors: ['#f48024', '#69d2e7'],
	// plotOptions: {
	// 	bar: {
	// 		columnWidth: '45%',
	// 		distributed: true,
	// 	}
	// },
	// dataLabels: {
	// 	enabled: false
	// },
	// legend: {
	// 	show: false
	// },
	// xaxis: {
	// 	categories: [
	// 		'Laki-Laki',
	// 		'Perempuan',
	// 	],
	// 	labels: {
	// 		style: {
	// 		colors: ['#f48024', '#69d2e7'],
	// 		fontSize: '12px'
	// 		}
	// 	}
	// }
};

var basiccolumnchart = new ApexCharts(document.querySelector("#basiccolumn"), basiccolumnoptions);
basiccolumnchart.render();

var basicpolarareachart = new ApexCharts(document.querySelector("#basicpolararea"), basicpolarareaoptions);
basicpolarareachart.render();

var custombarchart = new ApexCharts(document.querySelector("#custombar"), custombaroptions);
custombarchart.render();

var userdonatchart = new ApexCharts(document.querySelector("#userdonat"), userdonatoptions);
userdonatchart.render();

var simplepiechart = new ApexCharts(document.querySelector("#simplepie"), simplepieoptions);
simplepiechart.render();

var donateupdatechart = new ApexCharts(document.querySelector("#donateupdate"), donateupdateoptions);
donateupdatechart.render();

var radarmultiplechart = new ApexCharts(document.querySelector("#radarmultiple"), radarmultipleoptions);
radarmultiplechart.render();

var mixedlinecolumnchart = new ApexCharts(document.querySelector("#mixedlinecolumn"), mixedlinecolumnoptions);
mixedlinecolumnchart.render();

var radialwithimagechart = new ApexCharts(document.querySelector("#radialwithimage"), radialwithimageoptions);
radialwithimagechart.render();

var radialgradientchart = new ApexCharts(document.querySelector("#radialgradient"), radialgradientoptions);
radialgradientchart.render();

var radialcustomangelchart = new ApexCharts(document.querySelector("#radialcustomangel"), radialcustomangeloptions);
radialcustomangelchart.render();