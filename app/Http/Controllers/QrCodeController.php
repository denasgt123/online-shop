<?php

namespace App\Http\Controllers;

use App\Models\Qrcode;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;

class QrCodeController extends Controller
{
	public function index()
	{
		// Gate::authorize('admin');
		return view('qrcode');
	}

	public function create()
	{
		// Gate::authorize('admin');
		return view('auth.admin.create-qrcode')->with(['qrcode' => 'Wow']);
	}

	public function store(Request $request)
	{
		$request->validate([
			'qrcode' => 'required',
		]);

		$qrcode = new Qrcode($request->except('_token'));
		$qrcode->save();

		return view('auth.admin.create-qrcode')->with(['qrcode' => $request->qrcode]);
	}
}
