<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
	public function index()
	{
		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->latest()->take(5)->get();

		return view('auth.customer.notifikasi')->with(['notif' => $notif, 'jumlah' => $notif->count()]);
	}

	public function create()
	{
		return view('auth.admin.create_notifikasi');
	}

	public function store(Request $request)
	{
		// dd($request->link);
		$request->validate([
			'judul' => 'required',
			'isi_pesan' => 'required',
			'link' => ['string', 'nullable']
		]);

		$notif = new Notification($request->except('_token'));
		// $notif->judul = $request->judul;
		$notif->id_admin = Auth::user()->id;
		$notif->timestamps = now();
		$notif->save();

		return redirect()->route('admin.create-notifikasi');
	}

	public function getNotifCustomer()
	{
		// $data = Notification::where(function ($query) {
		// 	$query->where('id_customer', NULL)->whereNotNull('id_admin');
		// })->latest()->take(1)->get();
		// $notif = $data->mergeRecursive(Notification::where(function ($query) {
		// 	$query->where('id_customer', Auth::id())->whereNotNull('id_admin');
		// })->latest()->take(1)->get());

		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->oldest()->whereDoesntHave('users', function ($query) {
			$query->where('readed_notifications.id_customer', Auth::id());
		})->take(1)->get();

		if ($notif) {
			$notif[0]->users()->attach(Auth::id());
			return response()->json($notif[0]);
			// dd($data);
		}

		// return response()->json($notif[0]);
		// foreach ($notif as $data) {
		// 	if ($data->users->id_notification === $data->id){

		// 	}
		// }

		// $readed = Notification::all();
		// $readed = $readed->users();

		// $notif = Notification::find(1);
		// dd($readed);
		// dd($notif);
		// if($notif[0]->id === )
		// dd($notif[0]->users[0]->pivot->id_notification);

		// foreach ($notif->users as $test) {
		// 	dd($test->pivot->id_notification);
		// }
		// $notif = Notification::all();
		// if ($notif[$notif->count() - 1]->dibaca == 0) {
		// 	$data = $notif[$notif->count() - 1];
		// 	// $data->dibaca = 1;
		// 	$data->save();
		// 	return response()->json($data);
		// }
		return false;
	}

	public function setnotif($id)
	{
		$notif = new Notification();
		$notif->judul = "Ada Pembelian dari " . Auth::user()->nama;
		$notif->isi_pesan = Auth::user()->nama . " sudah melakukan checkout pembelian, harap segera acc !!";
		$notif->id_customer = $id;
		$notif->timestamps = now();
		$notif->save();

		return redirect()->route('customer.home');
	}
}
