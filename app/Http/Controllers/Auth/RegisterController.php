<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */

	// protected $redirectTo = RouteServiceProvider::HOME;
	protected $redirectTo = '/customer';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'nama' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'asal_kota' => ['required', 'string'],
			'gender' => ['required', 'integer'],
			'password' => ['required', 'string', 'min:8', 'confirmed'],
		]);
	}

	// public function defaultpage()
	// {
	// 	if (Auth::user()->role->nama === 'superadmin') {
	// 		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/superadmin';
	// 	} elseif (Auth::user()->role->nama === 'admin') {
	// 		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
	// 		// $this->redirectTo = '/admin';
	// 	} else {
	// 		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/customer';
	// 		// $this->redirectTo = '/customer';
	// 	}
	// }

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\Models\User
	 */
	protected function create(array $data)
	{
		// $this->defaultpage();
		return User::create([
			'nama' => $data['nama'],
			'email' => $data['email'],
			'asal_kota' => $data['asal_kota'],
			'id_role' => 3,
			'gender' => $data['gender'],
			'password' => Hash::make($data['password']),
		]);
	}
}
