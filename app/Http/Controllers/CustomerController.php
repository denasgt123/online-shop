<?php

namespace App\Http\Controllers;

use App\Models\DetailOrder;
use App\Models\Notification;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
	protected $notif;

	public function __construct()
	{
		$this->middleware(['auth', 'customer']);
		$this->middleware(function ($request, $next) {
			$this->notif = Notification::whereNotNull('id_admin')->where(function ($query) {
				$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
			})->latest()->take(5)->get();

			return $next($request);
		});

		// dd(Auth::guard('web')->check());
	}

	public function index()
	{
		// $notif = $this->getNotif();
		// $notif = Notification::whereNotNull('id_admin')->where(function ($query) {
		// 	$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		// })->latest()->take(5)->get();

		return view('auth.customer.index')->with(['notif' => $this->notif]);
		// return view('auth.customer.index')->with(['notif' => $notif, 'jumlah' => $notif->count()]);
		// return view('auth.customer.index');
	}

	public function indexCart()
	{
		// $notif = Notification::whereNotNull('id_admin')->where(function ($query) {
		// 	$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		// })->latest()->take(5)->get();

		return view('auth.customer.cart')->with(['notif' => $this->notif, 'jumlah' => $this->notif->count()]);
		// return view('auth.customer.cart')->with(['notif' => $notif, 'jumlah' => $notif->count()]);
	}

	public function indexCheckout()
	{
		// $notif = Notification::whereNotNull('id_admin')->where(function ($query) {
		// 	$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		// })->latest()->take(5)->get();

		return view('auth.customer.checkout')->with(['notif' => $this->notif, 'jumlah' => $this->notif->count()]);
		// return view('auth.customer.checkout')->with(['notif' => $notif, 'jumlah' => $notif->count()]);
	}

	public function indexNotifikasi()
	{
		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->latest()->take(5)->get();

		// return view('auth.customer.checkout')->with(['notif' => $this->notif, 'jumlah' => $this->notif->count()]);
		return view('auth.customer.notifikasi')->with(['notif' => $notif, 'jumlah' => $notif->count()]);
	}

	public function buyNow($id)
	{
		$order = new Order();
		$order->id_customer = $id;
		$order->save();

		for ($i = 1; $i <= 2; $i++) {
			$detail_order = new DetailOrder();
			$detail_order->id_product = $i;
			$detail_order->id_order = $order->id;
			$detail_order->jumlah = 1;
			$detail_order->save();
			$order->total += $detail_order->product->harga;
		}

		$order->save();

		$notif = new Notification();
		$notif->judul = "Ada Pembelian dari " . Auth::user()->nama;
		$notif->isi_pesan = Auth::user()->nama . " sudah melakukan checkout pembelian, harap segera acc !!";
		$notif->id_customer = $id;
		$notif->link = route('admin.list-transaksi');
		$notif->timestamps = now();
		$notif->save();

		return redirect()->route('customer.home');
	}

	public function getNotif()
	{
		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->whereDoesntHave('readed', function ($query) {
			$query->where('readed_notifications.id_customer', Auth::id());
		})->get()->count();

		if ($notif) {
			return response()->json($notif);
		} else {
			return response()->json(false);
		}
	}

	public function getNotifClick()
	{
		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->latest()->whereDoesntHave('readed', function ($query) {
			$query->where('readed_notifications.id_customer', Auth::id());
		})->take(5)->get();

		// dd(response()->json($notif));

		if (count($notif) > 0) {
			foreach ($notif as $data) {
				$data->readed()->attach(Auth::id());
			}
			return response()->json($notif);
		} else {
			return response()->json(false);
		}
	}

	public function getNotifPop()
	{
		$notif = Notification::whereNotNull('id_admin')->where(function ($query) {
			$query->where('id_customer', NULL)->orWhere('id_customer', Auth::id());
		})->oldest()->whereDoesntHave('poped', function ($query) {
			$query->where('pop_up_notifications.id_customer', Auth::id());
		})->take(1)->get();

		if (count($notif) > 0) {
			$notif[0]->poped()->attach(Auth::id());
			$notif[0]->dibaca = 0;
			return response()->json($notif[0]);
		} else {
			return response()->json(false);
		}
	}
}
