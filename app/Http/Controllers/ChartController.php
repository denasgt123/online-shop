<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ChartController extends Controller
{
	public function index()
	{
		$users_cowo = User::where('gender', '=', 1)->get()->count();
		$users_cewe = User::where('gender', '=', 2)->get()->count();

		return view('auth.admin.charts')->with(['cowo' => $users_cowo, 'cewe' => $users_cewe]);
	}
}
