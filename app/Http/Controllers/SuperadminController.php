<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SuperadminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('superadmin');
	}

	public function countgender($users)
	{
		return [
			'cowo' => $users->where('gender', 1)->count(),
			'cewe' => $users->where('gender', 2)->count(),
		];
	}

	public function getnotif()
	{
		$notif = Notification::all();
		return $notif[$notif->count() - 1];
	}

	public function index()
	{
		$users = $this->countgender(User::where('id_role', '=', 3)->get());

		return view('auth.superadmin.index')->with([
			'cowo' => $users['cowo'],
			'cewe' => $users['cewe'],
			'notif' => $this->getnotif(),
		]);
	}

	public function listAdmin()
	{
		$datas = User::where('id_role', '=', 2)->get();
		return view('auth.superadmin.list-admin', compact('datas'));
	}

	public function createAdmin()
	{
		return view('auth.superadmin.create-admin');
	}

	public function storeAdmin(Request $request)
	{
		$request->validate([
			'nama' => ['string', 'required'],
			'email' => ['email', 'required'],
			'gender' => ['numeric', 'required'],
			'password' => ['string', 'required']
		]);

		if ($request->password === $request->retypepassword) {
			$admin = new User($request->except('password'));
			$admin->id_role = 2;
			$admin->password = Hash::make($request->password);
			$admin->save();
			return redirect()->route('superadmin.list-admin');
		}
	}

	public function editAdmin(User $admin)
	{
		// dd($admin);
		return view('auth.superadmin.create-admin', ['admin' => $admin]);
	}

	public function updateAdmin(Request $request, User $admin)
	{
		$request->validate([
			'nama' => ['string'],
			'email' => ['email'],
			'gender' => ['integer'],
			'password' => ['string', 'nullable']
		]);

		// dd(isset($request->password));
		if (isset($request->password)) {
			if ($request->password === $request->retypepassword) {
				$admin->fill($request->except('password'));
				$admin->password = Hash::make($request->password);
				$admin->save();
				return $this->index();
			}
		} else {
			$admin->fill($request->except('password'));
			$admin->save();
			return redirect()->route('superadmin.list-admin');
		}
	}

	public function destroyAdmin(User $admin)
	{
		// dd($admin);
		$admin->delete();
		return back();
	}

	public function deleteCustomer(User $customer)
	{
		$customer->delete();
		return back();
	}
}
