<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Order;
use App\Models\Product;
use App\Models\Qrcode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
	protected $notif;

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
		$this->notif = Notification::whereNotNull('id_customer')->whereNull('id_admin')->take(5)->get();
		// dd($this->notif);
		// $this->notif = Notification::whereNotNull('id_customer')->whereNull('id_admin')->latest()->take(5)->get();
	}

	public function countgender($users)
	{
		return [
			'cowo' => $users->where('gender', 1)->count(),
			'cewe' => $users->where('gender', 2)->count(),
		];
	}

	public function getNotif()
	{
		$notif = Notification::whereNotNull('id_customer')->whereNull('id_admin')->where('dibaca', 0)->get()->count();
		// dd(response()->json($notif));
		if ($notif) {
			return response()->json($notif);
		}
		return response()->json(false);
	}

	public function getNotifClick()
	{
		$notif = Notification::whereNotNull('id_customer')->whereNull('id_admin')->where('dibaca', 0)->latest()->take(5)->get();
		if (count($notif) > 0) {
			foreach ($notif as $data) {
				$data->dibaca = 1;
				$data->save();
			}
			return response()->json($notif);
		}
		return response()->json(false);
	}

	public function getNotifPop()
	{
		$notif = Notification::whereNotNull('id_customer')->whereNull('id_admin')->where('popup', 0)->oldest()->take(1)->get();
		if (count($notif) > 0) {
			$notif[0]->popup = 1;
			$notif[0]->save();
			// dd(response()->json($notif[0]));
			return response()->json($notif[0]);
		}
		return response()->json(false);
	}

	public function index()
	{
		$users = $this->countgender(User::where('id_role', '=', 3)->get());
		return view('auth.admin.index')->with([
			'cowo' => $users['cowo'],
			'cewe' => $users['cewe'],
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
		// $datas = User::where('id_role', '=', 2)->get();
		// return view('auth.superadmin.list-admin', compact('datas'));
	}

	public function indexChart()
	{
		$users = $this->countgender(User::where('id_role', '=', 3)->get());
		$kota['surabaya'] = $this->countgender(User::where('id_role', 3)->where('asal_kota', 'Surabaya')->get());
		$kota['sidoarjo'] = $this->countgender(User::where('id_role', 3)->where('asal_kota', 'Sidoarjo')->get());
		$kota['mojokerto'] = $this->countgender(User::where('id_role', 3)->where('asal_kota', 'Mojokerto')->get());

		// dd($kota);
		return view('auth.admin.charts')->with([
			'genders' => $users,
			'kota' => json_encode($kota),
			// 'cowo' => $users['cowo'],
			// 'cewe' => $users['cewe'],
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
	}

	public function listProduct()
	{
		$products = Product::all();
		return view('auth.admin.list-product')->with(['products' => $products]);
	}

	public function createProduct()
	{
		return view('auth.admin.create-product');
	}

	public function storeProduct(Request $request)
	{
		// dd($request);
		$request->validate([
			'nama' => 'required',
			'deskripsi' => 'required',
			'harga' => ['required', 'numeric'],
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|nullable',
		]);

		$product = new Product($request->except(['_token', 'gambar']));
		if ($request->hasFile('gambar')) {
			// dd('test');
			$image = $request->file('gambar');
			$image_name = now() . '-' . $image->getClientOriginalName() . '.' . $image->getClientOriginalExtension();

			Storage::disk('public')->put('product/', $image);
			$product->gambar = $image_name;
		}
		$product->save();

		return redirect()->route('admin.list-product');
	}

	public function editProduct(Product $product)
	{
		// $products = Product::all();
		return view('auth.admin.list-product')->with(['product' => $product]);
	}

	public function updateProduct(Request $request, Product $product)
	{
		// $products = Product::all();
		// return view('auth.admin.list-product')->with(['products' => $products]);
	}

	public function deleteProduct(Product $product)
	{
		// $products = Product::all();
		// return view('auth.admin.list-product')->with(['products' => $products]);
	}

	public function createNotifikasi()
	{
		return view('auth.admin.create_notifikasi')->with([
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
	}

	public function storeNotifikasi(Request $request)
	{
		$request->validate([
			'judul' => 'required',
			'isi_pesan' => 'required',
			'link' => ['string', 'nullable']
		]);

		$notif = new Notification($request->except('_token'));
		$notif->id_admin = Auth::id();
		$notif->timestamps = now();
		$notif->save();

		return redirect()->route('admin.create-notifikasi');
	}

	public function createQrCode()
	{
		$qrcode = Qrcode::latest()->take(1)->get();
		$qrcodes = Qrcode::latest()->get();

		// dd($qrcodes->count());

		// if ($qrcode->count() !== 0 && $qrcodes->count() !== 0) {
		// 	return view('auth.admin.create-qrcode')->with([
		// 		'qrcode' => $qrcode[0] ? $qrcode[0]->qrcode : 'Wow',
		// 		'qrcodes' => $qrcodes->count() != 0 ? $qrcodes : NULL,
		// 		'notif' => $this->notif,
		// 		'jmlh_notif' => $this->notif->count(),
		// 	]);
		// } else {
		// 	return view('auth.admin.create-qrcode')->with([
		// 		'qrcode' => $qrcode[0] ? $qrcode[0]->qrcode : 'Wow',
		// 		'qrcodes' => $qrcodes->count() != 0 ? $qrcodes : NULL,
		// 		'notif' => $this->notif,
		// 		'jmlh_notif' => $this->notif->count(),
		// 	]);
		// }
		return view('auth.admin.create-qrcode')->with([
			'qrcode' => $qrcode->count() != 0 ? $qrcode[0]->qrcode : 'Wow',
			'qrcodes' => $qrcodes->count() != 0 ? $qrcodes : NULL,
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
	}

	public function storeQrCode(Request $request)
	{
		$request->validate([
			'qrcode' => 'required',
		]);

		$qrcode = new Qrcode($request->except('_token'));
		$qrcode->save();

		// return view('auth.admin.create-qrcode')->with(['qrcode' => $request->qrcode]);
		return redirect()->route('admin.create-qrcode');
	}

	public function listTransaksi()
	{
		$orders = Order::all();

		return view('auth.admin.list-transaction')->with([
			'orders' => $orders,
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
	}

	public function listCustomer()
	{
		$datas = User::where('id_role', '=', 3)->get();
		return view('auth.admin.list-customer', compact('datas'))->with([
			'notif' => $this->notif,
			'jmlh_notif' => $this->notif->count(),
		]);
	}

	public function deleteCustomer(User $customer)
	{
		$customer->delete();
		return back();
	}

	public function approve($id_admin, $id_order)
	{
		$order = Order::find($id_order);
		// dd($order);
		$order->is_success = 1;
		$order->save();

		$notif = new Notification();
		$notif->judul = "Order telah di proses !!";
		$notif->isi_pesan = "Ordermu telah disetujui oleh Admin " . Auth::user()->nama . ", Terima kasih telah belanja !!";
		$notif->id_admin = $id_admin;
		$notif->id_customer = $order->id_customer;
		$notif->timestamps = now();
		$notif->save();

		return redirect()->route('admin.list-transaksi');
	}
}
