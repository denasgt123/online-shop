<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	// protected $fillable = [
	// 	'judul',
	// 	'isi_pesan',
	// 	'id_admin',
	// 	'id_customer',
	// 	'id_customer',
	// ];

	protected $guarded = [];

	use HasFactory;

	/**
	 * Get all of the readed for the Notification
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	// public function readed()
	// {
	// 	return $this->hasMany(ReadedNotification::class, 'notification_id', 'id');
	// }

	public function readed()
	{
		return $this->belongsToMany(User::class, 'readed_notifications', 'id_notification', 'id_customer');
	}

	public function poped()
	{
		return $this->belongsToMany(User::class, 'pop_up_notifications', 'id_notification', 'id_customer');
	}
}
