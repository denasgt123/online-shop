<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	protected $guarded = [];
	public $timestamps = false;

	use HasFactory;

	public function detail()
	{
		return $this->hasMany(DetailOrder::class, 'id_order');
	}

	public function customer()
	{
		return $this->belongsTo(User::class, 'id_customer');
	}
}
