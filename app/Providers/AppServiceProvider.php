<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// 	Gate::define('admin', function(User $user) {
		// 		return $user->id_role === 2;
		// 	});
		Blade::directive('currency', function ($expression) {
			return "Rp. <?php echo number_format($expression,0,',','.'); ?>";
		});

		// if ($this->app->environment('production')) {
		URL::forceScheme('https');
		// }
	}
}
