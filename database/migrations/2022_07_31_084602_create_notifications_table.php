<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function (Blueprint $table) {
			$table->id();
			$table->integer('id_admin')->nullable();
			$table->integer('id_customer')->nullable();
			// $table->integer('id_user');
			$table->string('judul');
			$table->string('isi_pesan');
			$table->string('link')->nullable();
			$table->boolean('dibaca')->default(0);
			$table->boolean('popup')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('notifications');
	}
}
