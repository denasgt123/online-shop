<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$datas = [
			[
				'nama' => 'Product 1',
				'deskripsi' => "Some quick example text to build on the card title and make up the bulk of the card's content.",
				'harga' => 100000,
			],
			[
				'nama' => 'Product 2',
				'deskripsi' => "Some quick example text to build on the card title and make up the bulk of the card's content.",
				'harga' => 100000,
			],
		];

		foreach ($datas as $key => $value) {
			DB::table('products')->insert($value);
		}
	}
}
