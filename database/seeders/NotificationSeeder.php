<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$datas = [
			[
				'id_admin' => '3',
				'judul' => 'New Message from Denas2!',
				'isi_pesan' => 'Hello World, Have a good day! Wish you all the best....',
			],
			[
				'id_admin' => '4',
				'judul' => 'New Message from Admin!',
				'isi_pesan' => 'Hello World, Have a good day! Wish you all the best....',
			],
			[
				'id_admin' => '4',
				'judul' => 'New Message from Admin Again!',
				'isi_pesan' => 'Hello World, Have a good day! Wish you all the best....',
			],
			[
				'id_admin' => '3',
				'id_customer' => '5',
				'judul' => 'New Message from Denas2!',
				'isi_pesan' => 'Hello Denas3, Have a good day! Wish you all the best....',
			],
			[
				'id_admin' => '3',
				'id_customer' => '6',
				'judul' => 'New Message from Denas2!',
				'isi_pesan' => 'Hello Customer, Have a good day! Wish you all the best....',
			],
			[
				'id_customer' => '5',
				'judul' => 'New Message from Denas3!',
				'isi_pesan' => 'Hello World, Have a good day! Wish you all the best....',
			],
			// [
			// 	'id_admin' => '3',
			// 	'id_customer' => '5',
			// 	'judul' => 'New Message from Denas!',
			// 	'isi_pesan' => 'Hello World, Have a good day! Wish you all the best....',
			// ],
		];

		foreach ($datas as $key => $value) {
			DB::table('notifications')->insert($value);
		}
	}
}
