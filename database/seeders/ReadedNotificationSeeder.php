<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReadedNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$datas = [
			[
				'id_notification' => '1',
				'id_customer' => '5',
			],
			[
				'id_notification' => '1',
				'id_customer' => '6',
			],
		];

		foreach ($datas as $key => $value) {
			DB::table('readed_notifications')->insert($value);
		}
    }
}
