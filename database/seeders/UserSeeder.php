<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$datas = [
			// Superadmins
			[
				'nama' => 'Denassyah Nurrohman',
				'gender' => 1,
				'id_role' => 1,
				'email' => 'denas@denas.com',
				'password' => Hash::make('password'),

			],
			[
				'nama' => 'Superadmin',
				'gender' => 2,
				'id_role' => 1,
				'email' => 'superadmin@superadmin.com',
				'password' => Hash::make('password'),

			],

			// Admins
			[
				'nama' => 'Denassyah Nurrohman2',
				'gender' => 1,
				'id_role' => 2,
				'email' => 'denas2@denas.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Admin',
				'gender' => 2,
				'id_role' => 2,
				'email' => 'admin@admin.com',
				'password' => Hash::make('password'),
			],

			// Customers
			[
				'nama' => 'Denassyah Nurrohman3',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'denas3@denas.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer1',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer1@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer2',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer2@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer3',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer3@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer4',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer4@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer5',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer5@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer6',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer6@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer7',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer7@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer8',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer8@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer9',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer9@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer10',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer10@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer11',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer11@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer12',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer12@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer13',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer13@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer14',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer14@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer15',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer15@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer16',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer16@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer17',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer17@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer18',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer18@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer19',
				'gender' => 2,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer19@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer20',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer20@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer21',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer21@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer22',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer22@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer23',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer23@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer24',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer24@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer25',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer25@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer26',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer26@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer27',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer27@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer28',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Mojokerto',
				'email' => 'customer28@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer29',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Surabaya',
				'email' => 'customer29@customer.com',
				'password' => Hash::make('password'),
			],
			[
				'nama' => 'Customer30',
				'gender' => 1,
				'id_role' => 3,
				'asal_kota' => 'Sidoarjo',
				'email' => 'customer30@customer.com',
				'password' => Hash::make('password'),
			],
		];
		foreach ($datas as $key => $value) {
			DB::table('users')->insert($value);
		}
	}
}
