<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\QrCodeController;
use App\Http\Controllers\SuperadminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	return view('auth.login');
// });

Route::get('/', function () {
	return redirect()->route('login');
});

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/qrcode', [QrCodeController::class, 'index']);

// Route::middleware('guest')->group(function);
Route::name('superadmin.')->middleware('superadmin')->group(function () {
	Route::get('/superadmin', [SuperadminController::class, 'index'])->name('home');
	Route::get('/list-admin', [SuperadminController::class, 'listAdmin'])->name('list-admin');
	Route::get('/create-admin', [SuperadminController::class, 'createAdmin'])->name('create-admin');
	Route::post('/store-admin', [SuperadminController::class, 'storeAdmin'])->name('store-admin');
	Route::get('/edit-admin/{admin}', [SuperadminController::class, 'editAdmin'])->name('edit-admin');
	Route::put('/update-admin/{admin}', [SuperadminController::class, 'updateAdmin'])->name('update-admin');
	Route::delete('/delete-admin/{admin}', [SuperadminController::class, 'destroyAdmin'])->name('delete-admin');
	Route::delete('/delete-customer/{customer}', [SuperadminController::class, 'deleteCustomer'])->name('delete-customer');
});

Route::name('admin.')->middleware('admin')->group(function () {
	Route::get('/admin', [AdminController::class, 'index'])->name('home');
	Route::get('/charts', [AdminController::class, 'indexChart'])->name('charts');
	Route::get('/list-product', [AdminController::class, 'listProduct'])->name('list-product');
	Route::get('/create-product', [AdminController::class, 'createProduct'])->name('create-product');
	Route::post('/store-product', [AdminController::class, 'storeProduct'])->name('store-product');
	Route::get('/edit-product/{product}', [AdminController::class, 'editProduct'])->name('edit-product');
	Route::put('/update-product/{product}', [AdminController::class, 'updateProduct'])->name('update-product');
	Route::delete('/delete-product/{product}', [AdminController::class, 'deleteProduct'])->name('delete-product');
	Route::get('/list-customer', [AdminController::class, 'listCustomer'])->name('list-customer');
	Route::delete('/delete-customer/{customer}', [AdminController::class, 'deleteCustomer'])->name('delete-customer');
	Route::get('/create-notifikasi', [AdminController::class, 'createNotifikasi'])->name('create-notifikasi');
	Route::post('/store-notifikasi', [AdminController::class, 'storeNotifikasi'])->name('store-notifikasi');
	Route::get('/create-qrcode', [AdminController::class, 'createQrCode'])->name('create-qrcode');
	Route::post('/store-qrcode', [AdminController::class, 'storeQrCode'])->name('store-qrcode');
	Route::get('/list-transaksi', [AdminController::class, 'listTransaksi'])->name('list-transaksi');
	Route::get('/getnotifpopadmin', [AdminController::class, 'getNotifPop'])->name('getnotifpopadmin');
	Route::get('/getnotifadmin', [AdminController::class, 'getNotif'])->name('getnotifadmin');
	Route::get('/getnotifclickadmin', [AdminController::class, 'getNotifClick'])->name('getnotifclickadmin');
	Route::post('/approve/{id_admin}/{id_order}', [AdminController::class, 'approve'])->name('approve');

	// Route::get('/list-customer', [CustomerController::class, 'index'])->name('list-customer');
	// Route::delete('/delete-customer/{customer}', [CustomerController::class, 'destroy'])->name('delete-customer');
});

Route::name('customer.')->middleware('customer')->group(function () {
	Route::get('/customer', [CustomerController::class, 'index'])->name('home');
	Route::get('/cart', [CustomerController::class, 'indexCart'])->name('cart');
	Route::get('/checkout', [CustomerController::class, 'indexCheckout'])->name('checkout');
	Route::get('/notifikasi', [CustomerController::class, 'indexNotifikasi'])->name('notifikasi');
	Route::post('/buynow/{id}', [CustomerController::class, 'buyNow'])->name('buynow');
	Route::get('/getnotifpop', [CustomerController::class, 'getNotifPop'])->name('getnotifpop');
	Route::get('/getnotif', [CustomerController::class, 'getNotif'])->name('getnotif');
	Route::get('/getnotifclick', [CustomerController::class, 'getNotifClick'])->name('getnotifclick');
	// Route::get('/user/profile', function () {
	//     // Uses first & second middleware...
	// });
});

Auth::routes();
