@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <a href="{{ route('superadmin.list-admin') }}"><button class="btn btn-primary">Back</button></a>
                    <div class="d-flex justify-content-center">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ Route::is('superadmin.create-admin') ? 'Tambahkan' : 'Ubah' }} Admin</h4>
                            </div>
                            <div class="card-body">
                                <form
                                    action="{{ Route::is('superadmin.create-admin') ? route('superadmin.store-admin') : route('superadmin.update-admin', $admin->id) }}"
                                    method="post">
                                    @csrf
                                    @if (Route::is('superadmin.edit-admin'))
                                        @method('PUT')
                                    @endif
                                    <label for="nama">Nama</label>
                                    <input id="nama" type="text" name="nama"
                                        value="{{ Route::is('superadmin.create-admin') ? '' : $admin->nama }}" required>
                                    <label for="gender">Gender</label>
                                    <select id="gender" name="gender" required>
                                        <option value="1"
                                            {{ Route::is('superadmin.create-admin') ? '' : ($admin->gender === 1 ? 'selected' : '') }}>
                                            Laki-Laki</option>
                                        <option value="2"
                                            {{ Route::is('superadmin.create-admin') ? '' : ($admin->gender === 2 ? 'selected' : '') }}>
                                            Perempuan</option>
                                    </select>
                                    <label for="email">Email</label>
                                    <input id="email" type="email" name="email" required
                                        value="{{ Route::is('superadmin.create-admin') ? '' : $admin->email }}">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" name="password"
                                        {{ Route::is('superadmin.create-admin') ? 'required' : '' }}>
                                    <label for="retype-password">Retype Password</label>
                                    <input id="retype-password" type="password" name="retypepassword"
                                        {{ Route::is('superadmin.create-admin') ? 'required' : '' }}>
                                    <button
                                        type="submit">{{ Route::is('superadmin.create-admin') ? 'Tambah' : 'Ubah' }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
