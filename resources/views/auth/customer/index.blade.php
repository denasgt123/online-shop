@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12 d-flex justify-content-center">
                <div class="card my-3" style="width: 18rem;">
                    <img src="{{ asset('img/vuexy/product.jpg') }}" class="card-img-top align-self-center" alt="..."
                        style="width: 10vw; min-width: 100px">
                    <div class="card-body">
                        <h5 class="card-title">Product 1</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        <p class="card-text">Harga: Rp. 100.000,00-</p>
                        <a href="{{ route('customer.cart') }}" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 d-flex justify-content-center">
                <div class="card my-3" style="width: 18rem;">
                    <img src="{{ asset('img/vuexy/product.jpg') }}" class="card-img-top align-self-center" alt="..."
                        style="width: 10vw; min-width: 100px">
                    <div class="card-body">
                        <h5 class="card-title">Product 2</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        <p class="card-text">Harga: Rp. 100.000,00-</p>
                        <a href="{{ route('customer.cart') }}" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 d-flex justify-content-center">
                <div class="card my-3" style="width: 18rem;">
                    <img src="{{ asset('img/vuexy/product.jpg') }}" class="card-img-top align-self-center" alt="..."
                        style="width: 10vw; min-width: 100px">
                    <div class="card-body">
                        <h5 class="card-title">Product 3</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        <p class="card-text">Harga: Rp. 100.000,00-</p>
                        <a href="{{ route('customer.cart') }}" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script>
        let jmlh = 0;

        function getNotif() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response.length > jmlh) {
                        jmlh = response.length;
						
                        console.log(jmlh);
                    }
                    // if (response) {
                    //     tampilnotifikasi(response);
                    // } else {
                    //     return false;
                    // }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotif", true);
            httpxml.send();
        }

        setInterval(function() {
            getNotif();
        }, 10000);

		$(document).ready(getNotif());
    </script> --}}

    {{-- <script>
        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                        });
                    }
                });
            }
        }


        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotifcustomer", true);
            httpxml.send();
        }

        setInterval(function() {
            AjaxFunction();
        }, 10000);
    </script> --}}
@endsection
