@extends('layouts.app')

@section('content')
    <div class="container px-lg-0 px-md-1 px-sm-2 px-3">
        <div class="row my-2 w-100">
            <div class="col-md-2 col-sm-12 d-flex justify-content-center">
                <img src="{{ asset('img/vuexy/product.jpg') }}" alt="..." style="width: 10vw; min-width: 100px">
            </div>
            <div class="col-lg-9 col-md-8 col-8 d-flex align-items-center">
                <div>
                    <h5 class="text-dark">Product 1</h5>
                    <p class="">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <p class="">Harga: Rp. 100.000,00-</p>
                    {{-- <a href="#" class="btn btn-primary">Buy Now</a> --}}
                </div>
            </div>
            <div class="col-lg-1 col-md-2 col-4">
                <h5>Jumlah Barang :</h5>
                <select name="jumlah" id="jumlah">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <div class="row my-2 w-100">
            <div class="col-md-2 col-sm-12 d-flex justify-content-center">
                <img src="{{ asset('img/vuexy/product.jpg') }}" alt="..." style="width: 10vw; min-width: 100px">
            </div>
            <div class="col-lg-9 col-md-8 col-8 d-flex align-items-center">
                <div>
                    <h5 class="text-dark">Product 2</h5>
                    <p class="">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <p class="">Harga: Rp. 100.000,00-</p>
                    {{-- <a href="#" class="btn btn-primary">Buy Now</a> --}}
                </div>
            </div>
            <div class="col-lg-1 col-md-2 col-4">
                <h5>Jumlah Barang :</h5>
                <select name="jumlah" id="jumlah">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <hr>
        <div class="row my-2 w-100">
            <div class="col-lg-11 col-md-10 col-12 d-flex align-items-center">
                <div class="pl-2">
                    <h5 class="text-dark">Total : </h5>
                    <p class="">Product 1 x 1</p>
                    <p class="">Product 2 x 1</p>
                    <p class="">Total Harga: Rp. 200.000,00-</p>
                </div>
            </div>
            <div class="col-lg-1 col-md-2 col-12">
                <a href="{{ route('customer.checkout') }}"><button class="btn btn-success">Checkout</button></a>
            </div>
        </div>
    </div>
@endsection
