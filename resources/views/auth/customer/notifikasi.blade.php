@extends('layouts.app')

@section('content')
    {{-- @dd($notif) --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <center>
                    <h1>Test Notifikasi aja</h1>
                </center>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script>
        // console.log(Notification.permission);
        navigator.serviceWorker.register('sw.js');

        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                // alert("we have permission!");
                // for (let i = 0; i < 10; i++) {
                //     setTimeout(showNotification, 2000);
                //     console.log(Notification.permission);
                // }
                // setTimeout(showNotification, 5000);
                // showNotification();
                navigator.serviceWorker.ready.then((registration) => {
                    // alert('Service Worker Registered');
                    // registration.showNotification('{{ $notif->judul }}', {
                    //     body: '{{ $notif->isi_pesan }}',
                    //     icon: "/logo.png",
                    // });
                    registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });

                    // setTimeout(showNotification, 1000);
                    // console.log(registration);
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            // alert('Service Worker Registered');
                            // registration.showNotification('{{ $notif->judul }}', {
                            //     body: '{{ $notif->isi_pesan }}',
                            //     icon: "/logo.png",
                            // });
                            registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            // setTimeout(showNotification, 1000);
                            // console.log(registration);
                        });
                        // setTimeout(showNotification, 1000);
                    }
                });
                // Notification.requestPermission().then(permission => {
                //     // console.log(permission);
                //     if (permission === "granted") {
                //         for (let i = 0; i < 10; i++) {	
                //             setTimeout(showNotification, 2000);
                //             console.log(Notification.permission);
                //         }
                //         // setTimeout(showNotification, 5000);
                //         // showNotification();
                //     }
                // });
            }
        }

        // function showNotification() {
        //     const notification = new Notification("New Message from Denas!", {
        //         body: "Hello World, Have a good day! Wish you all the best....",
        //         icon: "/logo.png",
        //     });

        //     notification.onclick = (e) => {
        //         // window.location.href = "https://www.google.com/"
        //         window.open('http://localhost/phpmyadmin/', '_blank');
        //     }
        // }
    </script> --}}
    {{-- <script type="text/javascript">
        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    // console.log(httpxml.responseText);
                    // console.log('test');
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        // console.log(response);
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                    // if (nowLength != lastLength) {
                    // document.getElementById("mytext1").innerHTML = response.length;
                    // document.getElementById("mytext2").innerHTML = response[parseInt(response.length) - 1]['text'];
                    // }
                }
            }

            httpxml.onreadystatechange = stateck;
            try {
                httpxml.open("GET", "/getnotif", true);
                httpxml.send();
            } catch (err) {
                // console.log(err);
            }
        }

        // function NotificationPopUp() {
        //     $.ajax({
        //         url: '/getnotif',
        //         success: function(result) {
        //             // if (result.isOk == false) alert(result.message);
        //             console.log(result);
        //         },
        //         async: true
        //     });
        // }

        setInterval(function() {
            AjaxFunction();
            // NotificationPopUp();
        }, 10000);
        // AjaxFunction();
    </script> --}}
@endsection
