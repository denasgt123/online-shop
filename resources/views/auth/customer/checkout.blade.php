@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Checkout</h1>
        <div class="row my-5">
            <div class="col-lg-9 col-md-8 col-12 d-flex justify-content-center">
                <div class="row w-100">
                    <div class="col-lg-3 col-md-4 col-12 mb-3 text-center">
                        <img src="{{ asset('img/vuexy/product.jpg') }}" alt="..." style="width: 11vw; min-width: 100px">
                    </div>
                    <div class="col-lg-7 col-md-5 col-8 mb-3">
                        <h5 class="text-dark">Product 1</h5>
                        <p class="">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        <p class="">Harga: Rp. 100.000,00-</p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 mb-3">
                        <h5>Jumlah : 1</h5>
                    </div>
                    {{-- <a href="#" class="btn btn-primary">Buy Now</a> --}}
                    <div class="col-lg-3 col-md-4 col-12 text-center">
                        <img src="{{ asset('img/vuexy/product.jpg') }}" alt="..."
                            style="width: 11vw; min-width: 100px">
                    </div>
                    <div class="col-lg-7 col-md-5 col-8 mb-3">
                        <h5 class="text-dark">Product 2</h5>
                        <p class="">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        <p class="">Harga: Rp. 100.000,00-</p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 mb-3">
                        <h5>Jumlah : 1</h5>
                    </div>
                    {{-- <a href="#" class="btn btn-primary">Buy Now</a> --}}
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-12 d-flex justify-content-center">
                <div class="pt-md-0 pt-5">
                    <h5 class="text-dark">Shipping Address : </h5>
                    <p class="text-dark mb-0">Nama Customer</p>
                    <p class="text-muted mb-0">08312394213</p>
                    <p class="text-muted">Jl. Klampis Harapan X No.17, Klampis Ngasem, Kec. Sukolilo, Kota SBY, Jawa Timur
                        60118</p>
                    <hr>
                    <h5 class="text-dark mb-3">Order Summary : </h5>
                    <p class="mb-0">Product 1 x 1 = Rp. 100.000,00-</p>
                    <p class="">Product 2 x 1 = Rp. 100.000,00-</p>
                    <hr>
                    <p class="">Total Harga: Rp. 200.000,00-</p>
                    <button class="btn btn-success btn-checkout">Buy Now</button>
                    {{-- <a href="{{ route('customer.setnotif') }}/" id="checkout"></a> --}}
                    {{-- <a href="#" class="btn btn-primary">Buy Now</a> --}}
                </div>
            </div>
        </div>
    </div>
    <form id="checkout" method="POST" action="{{ route('customer.buynow', Auth::id()) }}">
        @csrf
        @method('POST')
    </form>
@endsection

@section('script')
    <script>
        document.querySelector(".btn-checkout").addEventListener('click', function() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Buy now!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: 'Transaction Proceed!',
                        html: 'Your Transaction has been proceed.',
                        icon: 'success',
                        showConfirmButton: false,
                    });
                    setTimeout(function() {
                        $("#checkout").submit();
                        // document.getElementById("checkout").click();
                    }, 1700);
                }
            })
            // Swal.fire(
            //     'Good job!',
            //     'You clicked the button!',
            //     'success'
            // );
            // console.log('test');
        });
        // function setnotif(link) {
        //     $("#delete-form").attr('action', link);
        // }
    </script>
@endsection
