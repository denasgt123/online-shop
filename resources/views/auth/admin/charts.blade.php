@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <div class="row">
                        <div class="col-6 my-3">
                            <div id="radialcustomangel"></div>
                        </div>
                        <div class="col-6 my-3 px-5">
                            <div id="basicpolararea"></div>
                            {{-- <div id="radialgradient"></div> --}}
                        </div>
                        {{-- <div class="col-8 my-3">
                            <div id="mixedlinecolumn"></div>
                        </div>
                        <div class="col-4 my-3">
                            <div id="radialwithimage"></div>
                        </div>
                        <div class="col-8 my-3">
                            <div id="mixedlinecolumn"></div>
						</div>
                        <div class="col-4 my-3">
                            <div id="radialwithimage"></div>
                        </div>
                        <div class="col-7 my-3 px-5">
                            <div id="radarmultiple"></div>
                        </div> --}}
                        <div class="col-6 my-3 px-5 d-flex justify-content-center">
                            <div id="donateupdate"></div>
                            {{-- <div class="actions">
                                <button id="add">
                                    + ADD
                                </button>
                                <button id="remove">
                                    - REMOVE
                                </button>
                                <button id="randomize">
                                    RANDOMIZE
                                </button>
                                <button id="reset">
                                    RESET
                                </button>
                            </div> --}}
                        </div>
                        <div class="col-6 my-3 px-3 d-flex justify-content-center align-items-center">
                            <div id="simplepie"></div>
                        </div>
                        <div class="col-6 my-3 px-3 d-flex justify-content-center align-items-center">
                            <div id="custombar"></div>
                        </div>
                        {{-- <div class="col-4 my-3 px-3 d-flex justify-content-center align-items-center">
                            <div id="basicpolararea"></div>
                        </div> --}}
                        <div class="col-6 my-3 px-3 d-flex justify-content-center align-items-center">
                            <div id="basiccolumn"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> --}}
    <script>
        const cowo = parseInt("{{ $genders['cowo'] }}");
        const cewe = parseInt("{{ $genders['cewe'] }}");
        // let kota = <?php echo json_encode($kota); ?>;
        let kota = JSON.parse('{!! $kota !!}');
        // console.log('coba kota');
        // console.log(kota['surabaya']['cowo']);

        function appendData() {
            var arr = donateupdatechart.w.globals.series.slice()
            arr.push(Math.floor(Math.random() * (100 - 1 + 1)) + 1)
            return arr;
        }

        function removeData() {
            var arr = donateupdatechart.w.globals.series.slice()
            arr.pop()
            return arr;
        }

        function randomize() {
            return donateupdatechart.w.globals.series.map(function() {
                return Math.floor(Math.random() * (100 - 1 + 1)) + 1
            })
        }

        function reset() {
            return donateupdateoptions.series
        }

        document.querySelector("#randomize").addEventListener("click", function() {
            donateupdatechart.updateSeries(randomize())
        })

        document.querySelector("#add").addEventListener("click", function() {
            donateupdatechart.updateSeries(appendData())
        })

        document.querySelector("#remove").addEventListener("click", function() {
            donateupdatechart.updateSeries(removeData())
        })

        document.querySelector("#reset").addEventListener("click", function() {
            donateupdatechart.updateSeries(reset())
        })
    </script>
    {{-- <script>
        navigator.serviceWorker.register('sw.js');

        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    var notif = registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                    notif.onclick = function() {
                        window.open('https://www.youtube.com/', '_blank');
                    };
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            var notif = registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            notif.onclick = function() {
                                window.open('https://www.youtube.com/', '_blank');
                            };
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotifadmin", true);
            httpxml.send();
        }

        setInterval(function() {
            AjaxFunction();
        }, 10000);
    </script> --}}
    <script src="{{ asset('js/vuexy/pages/charts.js') }}"></script>
@endsection
