@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <a href="{{ route('admin.home') }}">
                        <button class="btn btn-primary">Back</button>
                    </a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($datas as $data)
                                    <tr>
                                        <td>
                                            {{-- <img src="../../../app-assets/images/icons/angular.svg" class="mr-75" height="20" width="20" alt="Angular"> --}}
                                            <span class="font-weight-bold">{{ $data->nama }}</span>
                                        </td>
                                        <td>{{ $data->gender === 1 ? 'Laki-Laki' : 'Perempuan' }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>
                                            <a class="text-success" href="#"
                                                onclick="delete_data('{{ route('admin.delete-customer', $data->id) }}')">
                                                <i data-feather="unlock" width="14" height="14"></i>
                                                <span>Unblock</span>
                                            </a>
                                            <a class="text-warning mx-2" href="#"
                                                onclick="delete_data('{{ route('admin.delete-customer', $data->id) }}')">
                                                <i data-feather="lock" width="14" height="14"></i>
                                                <span>Block</span>
                                            </a>
                                            @if (Auth::user()->role->nama == 'superadmin')
                                                <a class="text-danger" href="#"
                                                    onclick="delete_data('{{ route('admin.delete-customer', $data->id) }}')">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-trash">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path
                                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                        </path>
                                                    </svg>
                                                    <span>Delete</span>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="delete-form" method="POST" action="">
        @csrf
        @method('DELETE')
    </form>
@endsection

@section('script')
    <script>
        function delete_data(link) {
            $("#delete-form").attr('action', link);
            $("#delete-form").submit();
        }
    </script>

    {{-- <script>
        navigator.serviceWorker.register('sw.js');

        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    var notif = registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                    notif.onclick = function() {
                        window.open('https://www.youtube.com/', '_blank');
                    };
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            var notif = registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            notif.onclick = function() {
                                window.open('https://www.youtube.com/', '_blank');
                            };
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotifadmin", true);
            httpxml.send();
        }

        setInterval(function() {
            AjaxFunction();
        }, 10000);
    </script> --}}
@endsection
