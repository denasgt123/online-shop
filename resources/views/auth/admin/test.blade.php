<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <div class="row">
                        <div class="col-8 my-3">
                            <div id="radialcustomangel"></div>
                        </div>
                        <div class="col-4 my-3">
                            <div id="radialgradient"></div>
                        </div>
                        <div class="col-8 my-3">
                            <div id="mixedlinecolumn"></div>
                        </div>
                        <div class="col-4 my-3">
                            <div id="radialwithimage"></div>
                        </div>
                        <div class="col-8 my-3">
                            <div id="mixedlinecolumn"></div>
                        </div>
                        <div class="col-4 my-3">
                            <div id="radialwithimage"></div>
                        </div>
                        <div class="col-7 my-3 px-5">
                            <div id="radarmultiple"></div>
                        </div>
                        <div class="col-5 my-3 px-3">
                            <div id="donateupdate"></div>
                            <div class="actions">
                                <button id="add">
                                    + ADD
                                </button>
                                <button id="remove">
                                    - REMOVE
                                </button>
                                <button id="randomize">
                                    RANDOMIZE
                                </button>
                                <button id="reset">
                                    RESET
                                </button>
                            </div>
                        </div>
                        <div class="col-12 my-3 d-flex justify-content-center">
                            <div id="simplepie"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
        const cowo = parseInt('{{ $cowo }}');
        const cewe = parseInt('{{ $cewe }}');

        function appendData() {
            var arr = donateupdatechart.w.globals.series.slice()
            arr.push(Math.floor(Math.random() * (100 - 1 + 1)) + 1)
            return arr;
        }

        function removeData() {
            var arr = donateupdatechart.w.globals.series.slice()
            arr.pop()
            return arr;
        }

        function randomize() {
            return donateupdatechart.w.globals.series.map(function() {
                return Math.floor(Math.random() * (100 - 1 + 1)) + 1
            })
        }

        function reset() {
            return donateupdateoptions.series
        }

        document.querySelector("#randomize").addEventListener("click", function() {
            donateupdatechart.updateSeries(randomize())
        })

        document.querySelector("#add").addEventListener("click", function() {
            donateupdatechart.updateSeries(appendData())
        })

        document.querySelector("#remove").addEventListener("click", function() {
            donateupdatechart.updateSeries(removeData())
        })

        document.querySelector("#reset").addEventListener("click", function() {
            donateupdatechart.updateSeries(reset())
        })
    </script>
	<script src="{{ asset('js/vuexy/pages/charts.js') }}"></script>
</html>
