@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <a href="{{ route('admin.home') }}">
                        <button class="btn btn-primary">Back</button>
                    </a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Orders</th>
                                    <th>Total</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($orders as $order)
                                    <tr>
                                        <td>
                                            <span class="font-weight-bold">{{ $order->customer->nama }}</span>
                                        </td>
                                        <td>
                                            @foreach ($order->detail as $detail)
                                                {{ $detail->product->nama }} Qty : {{ $detail->jumlah }} <br>
                                            @endforeach
                                        </td>
                                        <td>@currency($order->total)</td>
                                        <td>
                                            @if ($order->is_success === 0)
                                                <a class="text-success approve"
                                                    href="{{ route('admin.approve', ['id_admin' => Auth::id(), 'id_order' => $order->id]) }}">
                                                    <i data-feather="check" width="14" height="14"></i>
                                                    <span>Approve</span>
                                                </a>
                                            @else
                                                <span class="text-success font-weight-bold"><i data-feather="check"
                                                        width="14" height="14"></i> Approved</span>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">DATA TIDAK DITEMUKAN !!</td>
                                    </tr>
                                @endforelse
                                {{-- <tr>
                                    <td>
                                        <span class="font-weight-bold">Customer</span>
                                    </td>
                                    <td>
                                        Product 1 Qty : 1 <br>
                                        Product 2 Qty : 1
                                    </td>
                                    <td>Rp. 200.000</td>
                                    <td>
                                        <a class="text-success btn-approve" href="#">
                                            <i data-feather="check" width="14" height="14"></i>
                                            <span>Approve</span>
                                        </a>
                                    </td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <form id="delete-form" method="POST" action="">
        @csrf
        @method('DELETE')
    </form> --}}
    <form id="approve" method="POST" action="#">
        @csrf
        @method('POST')
    </form>
@endsection

@section('script')
    {{-- <script>
        function delete_data(link) {
            $("#delete-form").attr('action', link);
            $("#delete-form").submit();
        }
    </script> --}}

    {{-- <script>
        navigator.serviceWorker.register('sw.js');

        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    var notif = registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                    notif.onclick = function() {
                        window.open('https://www.youtube.com/', '_blank');
                    };
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            var notif = registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            notif.onclick = function() {
                                window.open('https://www.youtube.com/', '_blank');
                            };
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotifadmin", true);
            httpxml.send();
        }

        setInterval(function() {
            AjaxFunction();
        }, 10000);
    </script> --}}

    <script>
        document.querySelectorAll(".approve").forEach((el) => {
            el.addEventListener('click', function(e) {
                e.preventDefault();
                var link = this.href;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approve this!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            title: 'Order Proceed!',
                            html: 'Your Approval has been proceed.',
                            icon: 'success',
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            $("#approve").attr('action', link);
                            $("#approve").submit();
                        }, 1700);
                    }
                })
            });
        });
    </script>
@endsection