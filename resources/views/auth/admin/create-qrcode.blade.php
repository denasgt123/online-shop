@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body mb-5 pb-5">
                <div class="card">
                    <a href="{{ route('superadmin.home') }}"><button class="btn btn-primary">Back</button></a>
                    <div class="d-flex justify-content-center">
                        <div class="card">
                            <div class="card-header">
                                <h4>Buat Qrcode</h4>
                            </div>
                            <div class="card-body">
                                <form action="/store-qrcode" method="post">
                                    @csrf
                                    <label for="qrcode">Pesan QrCode : </label>
                                    <input id="qrcode" type="text" name="qrcode" required>
                                    <button type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h2>Simple QR Code</h2>
                            </div>
                            <div class="card-body">
                                {{ QrCode::size(300)->generate($qrcode) }}
                            </div>
                            <div class="card-footer">
                                <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                                    class="btn btn-primary" download="qrcode">Download</a>
                                {{-- <a href="#" class="btn btn-primary">Download</a> --}}
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h2>Color QR Code</h2>
                            </div>
                            <div class="card-body">
                                {{ QrCode::size(300)->backgroundColor(255, 90, 0)->generate($qrcode) }}
                            </div>
                            <div class="card-footer">
                                <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->backgroundColor(255, 90, 0)->generate($qrcode)) }}"
                                    class="btn btn-primary" download="qrcode">Download</a>
                                {{-- <a href="#" class="btn btn-primary">Download</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
                @if (isset($qrcodes))
                    <div class="card">
                        <div class="row">
                            <div class="col-4">
                                <h3 class="text-center mt-2 pt-1">Qrcode</h3>
                            </div>
                            <div class="col-6">
                                <h3 class="text-center mt-2 pt-1">Data Qrcode</h3>
                            </div>
                            <div class="col-2">
                                <h3 class="text-center mt-2 pt-1">Actions</h3>
                            </div>
                            @foreach ($qrcodes as $item)
                                <div class="col-4">
                                    <h3 class="text-center my-2">{{ QrCode::size(300)->generate($item->qrcode) }}</h3>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-center">
                                    <h4 class="text-center my-2">{{ $item->qrcode }}</h4>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center">
                                    <h3 class="text-center my-2">
                                        <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($item->qrcode)) }}"
                                            class="btn btn-primary" download="qrcode">Download</a>
                                        {{-- <a href="#" class="btn btn-primary">Download</a> --}}
                                    </h3>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
