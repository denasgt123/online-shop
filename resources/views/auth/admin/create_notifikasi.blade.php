@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <a href="{{ route('superadmin.home') }}"><button class="btn btn-primary">Back</button></a>
                    <div class="d-flex justify-content-center">
                        <div class="card">
                            <div class="card-header">
                                <h4>Input Notifikasi</h4>
                            </div>
                            <div class="card-body">
                                <form action="/store-notifikasi" method="post">
                                    @csrf
                                    <label for="judul">Judul Notifikasi : </label>
                                    <input id="judul" type="text" name="judul" required>
                                    <label for="isi">Isi Notifikasi : </label>
                                    <input id="isi" type="text" name="isi_pesan" required>
                                    <label for="link">Link : </label>
                                    <input id="link" type="text" name="link">
                                    <button type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script>
        navigator.serviceWorker.register('sw.js');

        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    var notif = registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                    notif.onclick = function() {
                        window.open('https://www.youtube.com/', '_blank');
                    };
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            var notif = registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            notif.onclick = function() {
                                window.open('https://www.youtube.com/', '_blank');
                            };
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/getnotifadmin", true);
            httpxml.send();
        }

        setInterval(function() {
            AjaxFunction();
        }, 10000);
    </script> --}}
@endsection
