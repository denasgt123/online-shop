@extends('layouts.app-admin')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="card">
                    <a href="{{ route('admin.list-product') }}"><button class="btn btn-primary">Back</button></a>
                    <div class="card">
                        <div class="card-header justify-content-center">
                            <h1>{{ Route::is('admin.create-product') ? 'Tambahkan' : 'Ubah' }}
                                Product</h1>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('img/vuexy/product.jpg') }}" alt=""
                                        style="max-height: 500px; max-width: 400px" id="preview">
                                </div>
                                <div class="col-8">
                                    <form
                                        action="{{ Route::is('admin.create-product') ? route('admin.store-product') : route('admin.update-product', $product->id) }}"
                                        method="post" class="pr-5" enctype="multipart/form-data">
                                        @csrf
                                        @if (Route::is('admin.edit-product'))
                                            @method('PUT')
                                        @endif
                                        <table class="w-100">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label for="nama">Nama</label>
                                                    </td>
                                                    <td>
                                                        <span>:</span>
                                                    </td>
                                                    <td>
                                                        <input id="nama" type="text" name="nama" class="w-100"
                                                            value="{{ Route::is('admin.create-product') ? '' : $product->nama }}"
                                                            required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="deskripsi">Deskripsi</label>
                                                    </td>
                                                    <td>
                                                        <span>:</span>
                                                    </td>
                                                    <td>
                                                        <textarea id="deskripsi" name="deskripsi" rows="4" cols="50" class="w-100" required>{{ Route::is('admin.create-product') ? '' : $product->deskripsi }}</textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="harga">Harga</label>
                                                    </td>
                                                    <td>
                                                        <span>:</span>
                                                    </td>
                                                    <td>
                                                        <input id="harga" type="number" name="harga" class="w-100"
                                                            value="{{ Route::is('admin.create-product') ? '' : $product->deskripsi }}"
                                                            required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="gambar">Gambar</label>
                                                    </td>
                                                    <td>
                                                        <span>:</span>
                                                    </td>
                                                    <td>
                                                        <input id="gambar" type="file" name="gambar"
                                                            onchange="previewImage()"
                                                            {{ Route::is('admin.create-product') ? 'required' : '' }}>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <button
                                            type="submit">{{ Route::is('admin.create-product') ? 'Tambah' : 'Ubah' }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function previewImage() {
            var preview = document.getElementById('preview');
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();

            reader.onloadend = function() {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>
@endsection
