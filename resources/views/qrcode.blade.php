@extends('layouts.app')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="container text-center">
                    <div class="card">
                        <div class="card-header">
                            <h2>Simple QR Code</h2>
                        </div>
                        <div class="card-body">
                            <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->generate('test')) }}"
                                alt="">
                            {{ QrCode::size(300)->generate('test') }}
                            <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->generate('test')) }}"
                                download="qrcode">Download</a>
                            {{-- {{ QrCode::size(300)->format('png')->generate('test') }} --}}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Color QR Code</h2>
                        </div>
                        <div class="card-body">
                            {{ QrCode::size(300)->backgroundColor(255, 90, 0)->generate('https://techvblogs.com/blog/generate-qr-code-laravel-8') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
