<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<!-- BEGIN: Head-->
@include('layouts.head-admin')
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern navbar-floating footer-static" data-open="click"
    data-menu="vertical-menu-modern" data-col="">

    <!-- BEGIN: Header-->
    @include('layouts.header-admin')
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    @include('layouts.sidebar-admin')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('layouts.footer-admin')
    <!-- END: Footer-->

    <!-- BEGIN: Script-->
    @include('layouts.script-admin')
    <!-- END: Script-->

    @if (Auth::user()->role->nama === 'admin')
        <!-- BEGIN: Notification JS-->
        <script>
            navigator.serviceWorker.register('sw.js');

            window.onload = function() {
                function tampilnotifikasi(data) {
                    if (Notification.premission === "granted") {
                        navigator.serviceWorker.ready.then((registration) => {
                            var notif = registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                            notif.onclick = function() {
                                window.open('https://www.youtube.com/', '_blank');
                            };
                        });
                    } else if (Notification.permission !== "denied") {
                        Notification.requestPermission(function(result) {
                            if (result === 'granted') {
                                navigator.serviceWorker.ready.then((registration) => {
                                    var notif = registration.showNotification(data['judul'], {
                                        body: data['isi_pesan'],
                                        icon: "/logo.png",
                                    });
                                    notif.onclick = function() {
                                        window.open('https://www.youtube.com/', '_blank');
                                    };
                                });
                            }
                        });
                    }
                }

                function countNotif() {
                    var httpxml = new XMLHttpRequest();

                    function stateck() {
                        if (httpxml.readyState == 4) {
                            var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                            if (response) {
                                let bell = document.getElementById("notif");
                                if (document.getElementById("jmlh-notif")) {
                                    document.getElementById("jmlh-notif").remove();
                                }
                                bell.insertAdjacentHTML("afterend",
                                    '<span class="badge badge-pill badge-danger badge-up" id="jmlh-notif">' + response +
                                    '</span>');
                            } else {
                                // document.getElementById("jmlh-notif").innerHTML.remove();
                                return false;
                            }
                        }
                    }
                    httpxml.onreadystatechange = stateck;
                    httpxml.open("GET", "/getnotifadmin", true);
                    httpxml.send();
                }

                function AjaxFunction() {
                    var httpxml = new XMLHttpRequest();

                    function stateck() {
                        if (httpxml.readyState == 4) {
                            var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                            if (response) {
                                tampilnotifikasi(response);
                            } else {
                                return false;
                            }
                        }
                    }
                    httpxml.onreadystatechange = stateck;
                    httpxml.open("GET", "/getnotifpopadmin", true);
                    httpxml.send();
                }

                countNotif();

                document.getElementById('notif').addEventListener('click', function(e) {
                    if (document.getElementById("jmlh-notif") != null) {
                        let jmlh_notif = document.getElementById("jmlh-notif").innerHTML;
                        if (jmlh_notif - 5 > 0) {
                            document.getElementById("jmlh-notif").innerHTML -= 5;
                        } else {
                            document.getElementById("jmlh-notif").remove();
                        }
                    }
                    var httpxml = new XMLHttpRequest();

                    function stateck() {
                        document.getElementById("notifications").innerHTML = "";
                        if (httpxml.readyState == 4) {
                            var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                            if (response) {
                                response.forEach(function(item, index) {
                                    document.getElementById("notifications").innerHTML +=
                                        '<a class="d-flex" href="' + (item['link'] ? item['link'] :
                                            'javascript:void(0)') +
                                        '"> <div class="media d-flex align-items-start"> <div class="media-left"> <div class="avatar bg-light-warning"> <div class="avatar-content"> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle avatar-icon"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg> </div> </div> </div> <div class="media-body"> <p class="media-heading"> <span class="font-weight-bolder">' +
                                        item['judul'] + '</span> </p> <small class="notification-text">' +
                                        item[
                                            'isi_pesan'] + '</small> </div> </div> </a>';
                                });
                            } else {
                                document.getElementById("notifications").innerHTML =
                                    '<p class="dropdown-item text-center">Tidak ada notifikasi<br></p>'
                            }
                        }
                    }
                    httpxml.onreadystatechange = stateck;
                    httpxml.open("GET", "/getnotifclickadmin", true);
                    httpxml.send();
                });

                setInterval(function() {
                    countNotif();
                    AjaxFunction();
                }, 10000);
            }
        </script>
        <!-- END: Notification JS-->
    @endif

    <!-- BEGIN: Page JS-->
    @yield('script')
    <!-- END: Page JS-->
</body>
<!-- END: Body-->

</html>
