<!-- BEGIN: Vendor JS-->
<script src="{{ asset('js/vuexy/vendors.min.js') }}"></script>
<script src="https://unpkg.com/feather-icons"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('js/vuexy/charts/apexcharts.min.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> --}}
{{-- <script src="{{ asset('js/vuexy/extensions/toastr.min.js') }}"></script> --}}
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('js/vuexy/core/app-menu.js') }}"></script>
<script src="{{ asset('js/vuexy/core/app.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: SWAL2 JS-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.26/dist/sweetalert2.all.min.js"></script>
<!-- END: SWAL2 JS-->

<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>
{{-- <script>
    navigator.serviceWorker.register('sw.js');

    if (Notification.premission === "granted") {
        navigator.serviceWorker.ready.then((registration) => {
            registration.showNotification('{{ $notif->judul }}', {
                body: '{{ $notif->isi_pesan }}',
                icon: "/logo.png",
            });
        });
    } else if (Notification.permission !== "denied") {
        Notification.requestPermission(function(result) {
            if (result === 'granted') {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification('{{ $notif->judul }}', {
                        body: '{{ $notif->isi_pesan }}',
                        icon: "/logo.png",
                    });
                });
            }
        });
    }
</script> --}}
