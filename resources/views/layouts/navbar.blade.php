<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>

        <div class="dropstart d-block d-md-none ms-auto me-3">
            <a class="notif" href="javascript:void(0);" data-toggle="dropdown" data-bs-toggle="dropdown"
                data-bs-display="static" id="notif">
                <i data-feather="bell"></i>
                {{-- <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger jmlh-notif"
                    id="jmlh-notif">2</span> --}}
            </a>
            <ul class="dropdown-menu dropdown-menu-start">
                <li class="dropdown-header mx-4">
                    <div class="d-flex justify-content-between">
                        <h4 class="notification-title mb-0 me-5">Notifications</h4>
                        <h4
                            class="badge rounded-pill bg-secondary bg-opacity-25 text-primary text-opacity-75 align-self-center ">
                            5 New
                        </h4>
                    </div>
                </li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li class="notifications" id="notifications">
                </li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li class="dropdown-menu-footer py-2 d-flex justify-content-center">
                    <a class="btn btn-primary btn-block dropdown-item" href="javascript:void(0)">Read
                        all notifications</a>
                </li>
            </ul>
        </div>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.home') }}">{{ __('Dashboard') }}</a>
                    </li>
                    {{-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('customer.notifikasi') }}">{{ __('Notifikasi') }}</a>
                        </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.cart') }}">{{ __('Cart') }}</a>
                    </li>
                    <li class="nav-item dropdown dropdown-notification mr-25 d-none d-sm-block">
                        {{-- <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown">
                                <i data-feather="bell"></i>
                                <span
                                    class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                    5
                                </span>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <div class="dropdown-header d-flex">
                                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                                            <div class="badge badge-pill badge-light-primary">6 New</div>
                                        </div>
                                    </li>
                                    <li class="scrollable-container media-list">
                                        <a class="d-flex" href="javascript:void(0)">
                                            <div class="media d-flex align-items-start">
                                                <div class="media-body">
                                                    <p class="media-heading">
                                                        <span class="font-weight-bolder">Congratulation Sam 🎉</span>winner!
                                                    </p>
                                                    <small class="notification-text"> Won the monthly best seller
                                                        badge.</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="dropdown-menu-footer"><a class="btn btn-primary btn-block"
                                            href="javascript:void(0)">Read all notifications</a></li>
                                </ul>
                            </a> --}}
                        <a class="nav-link notif" href="javascript:void(0);" data-bs-toggle="dropdown"
                            data-bs-auto-close="false" aria-expanded="false" id="notif">
                            <i data-feather="bell"></i>
                            {{-- <span
                                class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger jmlh-notif"
                                id="jmlh-notif"></span> --}}
                            {{-- @isset($jumlah)
                                    <span
                                        class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                        {{ $jumlah }}
                                    </span>
                                @endisset --}}
                        </a>
                        <ul class="dropdown-menu dropdown-menu-start">
                            {{-- @if (isset($notif))
                                    <li class="dropdown-header mx-4">
                                        <div class="d-flex justify-content-between">
                                            <h4 class="notification-title mb-0 me-5">Notifications</h4>
                                            @isset($jumlah)
                                                <h4
                                                    class="badge rounded-pill bg-secondary bg-opacity-25 text-primary text-opacity-75 align-self-center">
                                                    {{ $jumlah }} New</h4>
                                            @endisset
                                        </div>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Judul Notifikasi<br><small
                                            class="text-muted">Isi konten notifikasi
                                            yang akan ditampilkan ke notifikasi</small>
                                    </a>
									</li>
                                    <li id="notifications">
                                        <ul class="dropdown-item">

                                        </ul>
                                    </li>
                                    @forelse ($notif as $item)
                                        <li>
                                            <a class="dropdown-item pb-2" href="#">
                                                <span class="fs-5">{{ $item->judul }}</span><br>
                                                <small class="text-muted">{{ $item->isi_pesan }}</small>
                                            </a>
                                        </li>
                                    @empty
                                        <li>
                                            <p class="dropdown-item" href="#">Tidak ada notifikasi<br>
                                            </p>
                                        </li>
                                    @endforelse
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li class="dropdown-menu-footer py-2 d-flex justify-content-center">
                                        <a class="btn btn-primary btn-block dropdown-item" href="javascript:void(0)">Read
                                            all notifications</a>
                                    </li>
                                @else
                                    <li class="dropdown-header mx-4">
                                        <div class="d-flex justify-content-between">
                                            <h4 class="notification-title mb-0 me-5">Notifications</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li>
                                        <p class="dropdown-item" href="#">Tidak ada notifikasi<br>
                                        </p>
                                    </li>
                                @endif --}}
                            <li class="dropdown-header mx-4">
                                <div class="d-flex justify-content-between">
                                    <h4 class="notification-title mb-0 me-5">Notifications</h4>
                                    <h4
                                        class="badge rounded-pill bg-secondary bg-opacity-25 text-primary text-opacity-75 align-self-center ">
                                        5 New
                                    </h4>
                                </div>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li class="notifications" id="notifications">
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li class="dropdown-menu-footer py-2 d-flex justify-content-center">
                                <a class="btn btn-primary btn-block dropdown-item" href="javascript:void(0)">Read
                                    all notifications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->nama }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
