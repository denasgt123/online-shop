<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/feather-icons"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous"> --}}

    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('logo.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">

    <!-- PWA Chrome Android  -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="DPA">
    <link rel="icon" sizes="512x512" href="{{ asset('logo.png') }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="DPA">
    @yield('style')
</head>

<body class="vh-100 vw-100">
    <div id="app">
        @include('layouts.navbar')
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
    integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
</script>
{{-- <script src="{{ asset('/sw.js') }}"></script> --}}
{{-- Service Worker Init --}}
<script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
            console.log('test');
        });
    }
    console.log('test2');
</script>
{{-- Feather Icon Init --}}
<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.26/dist/sweetalert2.all.min.js"></script>
{{-- Notification Ajax --}}
{{-- <script>
    navigator.serviceWorker.register('/sw.js');

    Notification.requestPermission((result) => {
        if (result === 'granted') {
            navigator.serviceWorker.ready.then((registration) => {
                // Show a notification that includes an action titled Archive.
                registration.showNotification('New mail from Alice', {
                    actions: [{
                        action: 'archive',
                        title: 'Archive'
                    }]
                })
            });
        }
    });

    self.addEventListener('notificationclick', (event) => {
        event.notification.close();
        if (event.action === 'archive') {
            // User selected the Archive action.
            // archiveEmail();
            // console.log('click');
            clients.openWindow("https://youtu.be/PAvHeRGZ_lA");
        } else {
            // User selected (e.g., clicked in) the main body of notification.
            // clients.openWindow('/inbox');
            // console.log('click');
            clients.openWindow("https://youtu.be/PAvHeRGZ_lA");
        }
    }, false);
</script> --}}
@if (Auth::check())
    <script>
        // navigator.serviceWorker.register('/sw.js');

        // Notification.requestPermission((result) => {
        //     if (result === 'granted') {
        //         navigator.serviceWorker.ready.then((registration) => {
        //             // Show a notification that includes an action titled Archive.
        //             registration.showNotification('New mail from Alice', {
        //                 actions: [{
        //                     action: 'archive',
        //                     title: 'Archive'
        //                 }]
        //             })
        //         });
        //     }
        // });

        // self.addEventListener('notificationclick', (event) => {
        //     event.notification.close();
        //     if (event.action === 'archive') {
        //         // User selected the Archive action.
        //         // archiveEmail();
        //         // console.log('click');
        //         clients.openWindow("https://youtu.be/PAvHeRGZ_lA");
        //     } else {
        //         // User selected (e.g., clicked in) the main body of notification.
        //         // clients.openWindow('/inbox');
        //         // console.log('click');
        //         clients.openWindow("https://youtu.be/PAvHeRGZ_lA");
        //     }
        // }, false);

        document.querySelectorAll(".notif").forEach((el) => {
            el.addEventListener('click', function(e) {
                e.preventDefault();
                // console.log(document.getElementsByClassName("jmlh-notif"));
                document.querySelectorAll(".jmlh-notif").forEach(el => {
                    if (el) {
                        let jmlh_notif = el.innerHTML;
                        if (jmlh_notif - 5 > 0) {
                            el.innerHTML -= 5;
                        } else {
                            // document.getElementsByClassName("jmlh-notif").remove();
                            el.remove()
                        }
                    }
                });

                var httpxml = new XMLHttpRequest();

                function stateck() {
                    // document.getElementsByClassName("notifications").innerHTML = "";
                    // console.log(document.getElementsByClassName("notifications"));
                    // console.log(notif_node.innerHTML);
                    if (httpxml.readyState == 4) {
                        var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                        // console.log(response.length);
                        if (response) {
                            document.querySelectorAll(".notifications").forEach(el => {
                                el.innerHTML = "";
                                response.forEach(function(item, index) {
                                    // console.log(item);
                                    el.innerHTML +=
                                        '<a class="dropdown-item pb-2" href="' +
                                        (item['link'] ? item['link'] : '#') +
                                        '" target="_blank"> <span class="fs-5">' +
                                        item['judul'] +
                                        '</span><br> <small class="text-muted">' +
                                        item['isi_pesan'] +
                                        '</small> </a>';

                                    // document.getElementsByClassName("notifications").innerHTML +=
                                    //     '<a class="dropdown-item pb-2" href="' + (item['link'] ? item[
                                    //         'link'] : '#') + '" target="_blank"> <span class="fs-5">' +
                                    //     item['judul'] +
                                    //     '</span><br> <small class="text-muted">' + item['isi_pesan'] +
                                    //     '</small> </a>';
                                });
                            });
                        } else {
                            document.querySelectorAll(".notifications").forEach(el => {
                                el.innerHTML =
                                    '<p class="dropdown-item" href="#">Tidak ada notifikasi<br></p>';
                            });
                        }
                    }
                }
                httpxml.onreadystatechange = stateck;
                httpxml.open("GET", "/getnotifclick", true);
                httpxml.send();
            });
        });

        // document.querySelector('.notif').addEventListener('click', function(e) {
        //     if (document.getElementById("jmlh-notif") != null) {
        //         let jmlh_notif = document.getElementById("jmlh-notif").innerHTML;
        //         if (jmlh_notif - 5 > 0) {
        //             document.getElementById("jmlh-notif").innerHTML -= 5;
        //         } else {
        //             document.getElementById("jmlh-notif").remove();
        //         }
        //     }
        //     var httpxml = new XMLHttpRequest();

        //     function stateck() {
        //         document.getElementById("notifications").innerHTML = "";
        //         // console.log(document.getElementsByClassName("notifications"));
        //         // console.log(notif_node.innerHTML);
        //         if (httpxml.readyState == 4) {
        //             var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
        //             // console.log(response.length);
        //             if (response) {
        //                 response.forEach(function(item, index) {
        //                     // console.log(item);
        //                     document.getElementById("notifications").innerHTML +=
        //                         '<a class="dropdown-item pb-2" href="' + (item['link'] ? item[
        //                             'link'] : '#') + '" target="_blank"> <span class="fs-5">' +
        //                         item['judul'] +
        //                         '</span><br> <small class="text-muted">' + item['isi_pesan'] +
        //                         '</small> </a>';
        //                 });
        //             } else {
        //                 document.getElementById("notifications").innerHTML =
        //                     '<p class="dropdown-item" href="#">Tidak ada notifikasi<br></p>'
        //             }
        //         }
        //     }
        //     httpxml.onreadystatechange = stateck;
        //     httpxml.open("GET", "/getnotifclick", true);
        //     httpxml.send();
        // });

        window.onload = function() {
            function tampilnotifikasi(data) {
                if (Notification.premission === "granted") {
                    navigator.serviceWorker.ready.then((registration) => {
                        registration.showNotification(data['judul'], {
                            body: data['isi_pesan'],
                            icon: "/logo.png",
                        });
                        // notif.onclick = function() {
                        //     window.open('https://www.youtube.com/', '_blank');
                        // };
                        // notif.addEventListener('notificationclick', (event) => {
                        //     window.open('https://www.youtube.com/', '_blank');
                        // });
                    });

                    // self.addEventListener('notificationclick', (event) => {
                    //     event.notification.close();
                    //     window.open('https://www.youtube.com/', '_blank');
                    // });
                } else if (Notification.permission !== "denied") {
                    Notification.requestPermission(function(result) {
                        if (result === 'granted') {
                            navigator.serviceWorker.ready.then((registration) => {
                                registration.showNotification(data['judul'], {
                                    body: data['isi_pesan'],
                                    icon: "/logo.png",
                                });
                                // notif.onclick = function() {
                                //     window.open('https://www.youtube.com/', '_blank');
                                // };
                                // notif.addEventListener('notificationclick', (event) => {
                                //     window.open('https://www.youtube.com/', '_blank');
                                // });
                            });
                        }
                    });

                    // self.addEventListener('notificationclick', (event) => {
                    //     // event.notification.close();
                    //     // window.open('https://www.youtube.com/', '_blank');
                    //     // console.log('click');
                    // });
                }
            }

            function AjaxFunction() {
                var httpxml = new XMLHttpRequest();

                function stateck() {
                    if (httpxml.readyState == 4) {
                        var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                        if (response) {
                            tampilnotifikasi(response);
                        } else {
                            return false;
                        }
                    }
                }
                httpxml.onreadystatechange = stateck;
                httpxml.open("GET", "/getnotifpop", true);
                httpxml.send();
            }

            function countNotif() {
                var httpxml = new XMLHttpRequest();

                function stateck() {
                    if (httpxml.readyState == 4) {
                        var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                        if (response) {
                            // let bell = document.getElementById("notif");
                            document.querySelectorAll(".notif").forEach((el) => {
                                // console.log(el);
                                // if (document.getElementsByClassName("jmlh-notif")) {
                                //     $(".jmlh-notif").remove();
                                // }
                                if (el.childElementCount == 2) {
                                    el.lastChild.remove();
                                }

                                el.innerHTML +=
                                    '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger jmlh-notif" id="jmlh-notif">' +
                                    response + '</span>';
                                // el.insertAdjacentHTML("afterend",
                                //     '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger jmlh-notif">' +
                                //     response + '</span>');
                            });
                            // if (document.getElementById("jmlh-notif")) {
                            //     document.getElementById("jmlh-notif").remove();
                            // }
                            // bell.insertAdjacentHTML("afterend",
                            //     '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="jmlh-notif">' +
                            //     response + '</span>');
                            // document.getElementById("jmlh-notif").innerHTML =
                            //     '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">' +
                            //     response + '</span>';
                        } else {
                            // document.getElementById("jmlh-notif").innerHTML.remove();
                            return false;
                        }
                    }
                }
                httpxml.onreadystatechange = stateck;
                httpxml.open("GET", "/getnotif", true);
                httpxml.send();
            }
            countNotif();

            setInterval(function() {
                countNotif();
                AjaxFunction();
            }, 10000);
        }
    </script>
@endif

@yield('script')

</html>
